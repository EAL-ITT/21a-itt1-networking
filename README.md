![Build Status](https://gitlab.com/EAL-ITT/21a-itt1-networking/badges/master/pipeline.svg)


# 21A-ITT1-NETWORKING

weekly plans, resources and other relevant stuff for courses.

public website for students:

*  [gitlab pages](https://eal-itt.gitlab.io/21a-itt1-networking/)
