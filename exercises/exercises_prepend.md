---
title: '21A ITT1 NETWORKING'
subtitle: 'Exercises'
authors: ['Per Dahlstrøm \<pda@ucl.dk\>']
main_author: 'Per Dahlstrøm'
date: \today
email: 'pda@ucl.dk'
left-header: \today
right-header: '21A ITT1 NETWORKING, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans.

References

* [Weekly plans](https://eal-itt.gitlab.io/21a-itt1-networking/21A_ITT1_NETWORKING_weekly_plans.pdf)



