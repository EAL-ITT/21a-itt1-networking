---
Week: 2
Content: Review questions
Material: See links in weekly plan
Initials: PDA
---

# Week 2 Networking

## Learning goals

* Review questions.
* Multiple Choice exam. Please see more exams info in the Semester Descriptionn on ucl.dk. 

## Topics to be covered

* Review questions. Students ask question on topics where they feel insecure.

## Schedule

* See TimeEdit


## Sources

* See all previous Weekly plans ressources.
