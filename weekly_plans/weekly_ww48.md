---
Week: 48
Content:  Assignment 27. Routing, one router, two subnets. Physical router.
Material: See links in weekly plan
Initials: PDA
---

# Week 48 Networking

### Learning goals

The students can at a basic level work with and explain:

* Routing on physical router

### Topics to be covered

* Routing on a Juniper physical SRX240

## Documentation to hand in

Assignment 27 Routing on physical SRX240

- PeerGrade: Team hand in.   

    Please find the assignment here: 

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#assignment-27-routing-on-physical-srx240'" type="button"> Assignment 27</button>  

## Schedule

Please see TimeEdit.  

## Comments

Workload 8 hours. 

## Instructor Instruction Videoes  

* <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-10-one-router-two-subnets'" type="button"> Assignment 10 videoes</button>

## Lecturer text sources

* Network Lab B2.19  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#network-lab-b219/'" type="button"> Networking Lab </button> 

* Networking troubleshooting:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Troubleshooting</button>  

<!--
* 18A VMware Juniper Junos vSRX basic routing PDA V02.pdf  
Find it on ITSL in Resources

* Only Section 6.1 in this document:  
https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/Python_Network_programming_PDA_V08_p8-18.pdf

* ARP and routing.
https://www.youtube.com/watch?v=cn8Zxh9bPio

* IP program cheat sheet
https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/19A_IP_command_cheatsheet_PDA.pdf

* ISO OSI model  
https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/1_Datacommunication_OSI_model_V06.pdf
-->  

## Software

* Use Putty to access the router.  

## Configuration(s)

* Same configuration as assignment 10

* Please find the base configuration for this assignment here:

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/srx_configs/#_top'" type="button"> Configurations</button>  

