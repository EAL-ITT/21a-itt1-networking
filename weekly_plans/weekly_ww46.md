---
Week: 46
Content: Assignment 10. Routing, one router, two subnets. Virtual router.
Material: See links in weekly plan
Initials: PDA
---

# Week 46 Networking

## Lecturer plan

1. Peer review of previous assignnment.  
1. This weeks weekly plan.
1. K1 and K2. Introduction to assignment. Videoe(s)
1. K3 and K4. Students work on assignment.

## Learning goals

The (LGXX)) codes used below refere to the Learning Goals also listed in The Lecture Plan.

**Knowledge:**

The student has acquired a knowledge and an understanding of:

* (LGK1) What a router fundamentally is doing in a network.
* (LGK2) How to work in the Junos CLI. 
* (LGK3) Optional: ARP and MAC addresses in routing. 

**Skills:**

The student can:

* (LGS1) 

**Competences:**

The student is able to:

**After this assignment, the student can explain:**

* What a router fundamentally is doing in a network.  
* Fundamental routing. Static routes.  
* How to work in the Junos CLI.   
* Subnet. Not all the details.  
* A router interface as the Default Gateway for a network.  
* Broadcast domain.  
* Optional: ARP and MAC addresses in routing.    


### Topics to be covered

* Routing on a virtual Juniper SRX240 called vSRX
* Junos CLI

**Linux commands:**  

    $ arp See more commands in assignment in deliverables.  

## Documentation to hand in

Assignment 10 Routing, one router, two subnets. Virtual router.

- PeerGrade: Individual hand in, but work in teams.   

    Please find the assignment here: 

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#assignment-10-routing-one-router-two-subnets-virtual-router'" type="button"> Assignment 10</button>  

## Schedule

* Please see TimeEdit

## Workload

* 8 hours.  

## Instructor Instruction Videoes  

* <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-10-one-router-two-subnets'" type="button"> Assignment 10 videoes</button>

* Networking troubleshooting:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Troubleshooting</button>  

## External video sources  

* PING Command - Troubleshooting  

  <button onclick="location.href='https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos'" type="button"> Ping</button>  

* ARP and routing. This is with two routers. PLEASE note that Routing is ONE thing and ARP is another thing.  

  <button onclick="location.href='https://www.youtube.com/watch?v=cn8Zxh9bPios'" type="button"> ARP and Routing</button>  

## Software

* These three files constitute the vSRX router version 12 for installation on VMWW. Please download them from e.g. the ITSL folder: Resources.

      junos-vsrx-12.1X47-D15.4-domestic.mf  
      junos-vsrx-12.1X47-D15.4-domestic.ovf  
      junos-vsrx-12.1X47-D15.4-domestic-disk1.vmdk  

## Lecturer text sources

* Please find the following text sources in this GitLab repository:  

  [https://gitlab.com/PerPer/networking/-/tree/master/Semester_Literature/1_semester_network_literature](https://gitlab.com/PerPer/networking/-/tree/master/Semester_Literature/1_semester_network_literature) 

<!-->
* 18A VMware Juniper Junos vSRX basic routing PDA V0x.pdf  
* 19A_IP_command_cheatsheet_PDA.pdf  
* Only Section 6.1 in this document: Python_Network_programming_PDA_V08_p8-18.pdf  
* Regarding the CLI, chapter 2 in: JNCIA-IJOS-Junosphere-12.c-R_SG.pdf  


**Configuration(s)**

Please find the base configuration for this assignment here:

<button onclick="location.href='https://perper.gitlab.io/networkingpages/srx_configs/#_top'" type="button"> Configurations</button>  