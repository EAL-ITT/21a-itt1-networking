---
Week: 35 - 36
Content:  Assignment 3. VM Ware Workstation and Linux Virtual Machine installation.   
Material: See links in weekly plan
Initials: PDA
---

# Week 35 - 36

## Learning goals

* How to install VMware Workstation VMWW.
* Basic VMware Workstation understanding.
* Virtual Machine Xubuntu/Linux installation and conection to the internet.
* Superficially able to explain what a hardware emulator or Hypervisor is.
* Peer documentation review.

## Documentation to hand in

* Assignment 3, which is found in the assignment document: 21A_Networking_assignments_Vxx.pdf  

* Please find the Assignments documents in this GitLab folder:  
[https://gitlab.com/PerPer/networking/-/tree/master/Networking_Assignments](https://gitlab.com/PerPer/networking/-/tree/master/Networking_Assignments)

* It is obligatory to follw the "Hand in requirements" stipulated in the first section of the assignments document.

* Hand in on PeerGrade. A PeerGrade notification should have been send to you.  

* It is obligatory to use the MS Word lab report template found here for your hand in:
[https://gitlab.com/PerPer/networking/-/tree/master/Documentation_writing](https://gitlab.com/PerPer/networking/-/tree/master/Documentation_writing)


## Schedule

Please see TimeEdit. Lecturing always start at 9, unless notified on ITSL.

## Comments

* Workload: 8 hours.

This assignment is relatively big and will run over 1 weeks.

## Instructor Instruction Videoes

* Intstruction, theory and demonstration videos to assignment 3. 

* Part 1: Introduction to assignment 3.  

  [https://www.youtube.com/watch?v=Ynzr2ccSvww](https://www.youtube.com/watch?v=Ynzr2ccSvww)  

* Part 2: Install VMWare Workstation.  

   [https://www.youtube.com/watch?v=hA73HN-0Adk](https://www.youtube.com/watch?v=hA73HN-0Adk) 

* Part 3: Install your first Virtual Machine on Workstation. Xubuntu as it is leightweight.  

  [https://www.youtube.com/watch?v=gemkZn_BZJ0](https://www.youtube.com/watch?v=gemkZn_BZJ0)  
  
* Part 4: Configure the Xubuntu terminal layout and keyboard layout.  

  [https://www.youtube.com/watch?v=LclvpOM2maI](https://www.youtube.com/watch?v=LclvpOM2maI)

* Part 5: Connect your first Virtual Machine to the internet.  

  [https://www.youtube.com/watch?v=FF2U0qt9O5k](https://www.youtube.com/watch?v=FF2U0qt9O5k)  

* Part 6: What is VMWare Workstation. What is a Hypervisor?  

  Please find alternative online material on this topic.

  []()  

## External video sources

* Intstruction, theory and demonstration videos to assignment 3. 

* Create a Virtual Machine in VMware Workstation Pro Version 12.
A detailed walkthrough on how to install Lubuntu.  

  [https://www.youtube.com/watch?v=BHpRTVP8upg&ab_channel=danscourses](https://www.youtube.com/watch?v=BHpRTVP8upg&ab_channel=danscourses)  


## Instructor text sources

Please find the following text sources in this GitLab repository:  

  [https://gitlab.com/PerPer/networking/-/tree/master/Semester_Literature/2_semester_network_literature](https://gitlab.com/PerPer/networking/-/tree/master/Semester_Literature/2_semester_network_literature) 

* Linux networking tools and some troubleshooting:  
[https://gitlab.com/PerPer/networking/-/tree/master/Troubleshooting](https://gitlab.com/PerPer/networking/-/tree/master/Troubleshooting)

## External text sources

Please find the following text sources online:  


## Configuration(s)

* TBD

## Software

* VMware Workstation

  [https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html](https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html)

* Xubuntu

  TBD

  []()

## Lecturer lecture plan

What the lecturer will talk about on class:

* ITSL. Lecture plan and links to material on GitLab.
* GitLab.
* PeerGrade. E,g, review and hand in deadlines.
* OLA for Networking. Semester description.
* Assignments document .pdf on GitLab
* TimeEdit. Structure of the networking days.
* This weeks asignment. Assignment 3.
* The hand in format. I.e. the hand in word template document on GitLab.
* Theory/Videos for this weeks asignment. Assignment 3.