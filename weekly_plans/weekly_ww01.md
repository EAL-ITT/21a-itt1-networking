---
Week: 1
Content:  Assignment 28. Routing, two routers, five subnets. Physical routers.
Material: See links in weekly plan
Initials: PDA
---

# Week 1 Networking

## Learning goals

The students can at a basic level work with and explain:

* Routing between routers on physical routers.  

## Topics to be covered

* Routing between routers on physical routers.    

## Documentation to hand in

Assignment 28 Routing, two routers, five subnets, virtual routers.  

- PeerGrade: Individual hand in. Work in teams.  

    Please find the assignment here: 

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#assignment-28-routing-two-routers-five-subnets-physical-routers'" type="button"> Assignment 28</button>  

## Schedule

Please see TimeEdit.  

## Comments

Workload 8 hours.

## Software

Use Putty to access the routers. 

## Instructor Instruction Videoes  

* As the configurations here in assignment 28 are the same as for the virtual routers in assignment 11, the instructions for assignment 11 are valid for this assignment 28:  

* <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-11-two-routers-5-subnets'" type="button"> Assignment 11 videoes</button>

## Lecturer text sources

* Networking troubleshooting:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Troubleshooting</button>  
