---
Week: 45
Content:  Assignment 5. The switch and Spanning Tree Protocol. STP.
Material: See links in weekly plan
Initials: PDA
---

# Week 45 Networking

### Practical goals

* See Assignment in Deliverables

### Learning goals

The students can at a basic level work with and explain:

* Describe the Spanning Tree Protocol STP.
* Switch on and off the STP on a Linux based switch.
* Set up a small network in VMware Workstation.
* Use Wireshark to monitor relevant network traffic.

### Topics to be covered

* The ethernet layer 2 switch running the STP.
* Network Diagrams.
* MAC addresses
* IP adresses.
* Using Wireshark to monitor traffic.

Linux commands:  

    NA  

Windows commands:  

    NA

## Documentation to hand in

Assignment 5 The switch and STP

- PeerGrade: Team hand in.  

    Please find the assignment here: 

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#assignment-5-the-switch-and-stp'" type="button"> Assignment 5</button>  

## Schedule

Please see TimeEdit

## Comments

Workload: 8 hours.  

## Instructor Instruction Videoes

<button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignmen-5-spanning-tree-protocol-stp'" type="button"> Assignment 5 videoes</button>

* Networking troubleshooting:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Troubleshooting</button>  

## External video sources  

* STP Spanning Tree Protocoll explained (Only the first 07:11 minutes.)  
  [https://www.youtube.com/watch?v=japdEY1UKe4&ab_channel=CertBros](https://www.youtube.com/watch?v=japdEY1UKe4&ab_channel=CertBros)

  <button onclick="location.href='https://www.youtube.com/watch?v=japdEY1UKe4&ab_channel=CertBros'" type="button"> CertBros STP</button>

## Lecturer plan

1. Peer review of previous assignnment.  
1. Semester evaluation.  
1. This weeks weekly plan.
1. Introduction videoe(s) to assignment.
