---
Week: 38
Content:  Assignment 1. IP and MAC addresses. ARP table.    
Material: See links in weekly plan
Initials: PDA
---

# Week 38 Networking


## Learning goals

The students can at a very basic level work with and explain:

* Install a Raspberry Pi Buster Operating system on a Virtual Machine VM in VMWW.  
* Installation of Network Manager.
* Use ping to verify connectivity between network devices.
* IP and MAC addresses.
* Inspect the ARP table on a Linux box, here a Raspberry.
* Use Wireshark to confirm IP and MAC addresses.
 

### Topics to be covered

* Raspberry Pi Buster Operating system as a VM in VMWW. RPi base VM.  
* Installing Network Manager aka. nmcli.
* Two hosts - VM Raspberry Pi Buster clones.
* Set static IPs on Raspberry Pi in nmcli GUI.  
* MAC addresses.  
* The ping program.
* Basic IP packet traffic monitoring in Wireshark.

###  Linux Commands:  
  
    * $ ip neigh or $ arp  (Listing the ARP table with IP and MAC addresses.)
    * $ man ip  (Manual pages for the ip command or program.)
    * $ ping  
    * $ sudo wireshark  (Start Wireshark with access to interfaces.)
  
### Windows commands:  

    * TBD    

## Documentation to hand in  

* Assignment 1 Raspberry VM, VMW bridge, IP,  MAC addresses and ARP table.  

* Please find the Assignments documents in this GitLab folder:  
[https://gitlab.com/PerPer/networking/-/tree/master/Networking_Assignments](https://gitlab.com/PerPer/networking/-/tree/master/Networking_Assignments)

* It is obligatory to follw the "Hand in requirements" stipulated in the first section of the assignments document.

* Hand in on PeerGrade. A PeerGrade notification should have been send to you.  

* It is obligatory to use the MS Word lab report template found here for your hand in:
[https://gitlab.com/PerPer/networking/-/tree/master/Documentation_writing](https://gitlab.com/PerPer/networking/-/tree/master/Documentation_writing)

## Schedule

* Please see TimeEdit. Lecturing always start at 9, unless notified on ITSL.

## Instructor Instruction Videoes

* Intstruction, theory and demonstration videos to assignment 1.  
Please note that some of the videos are fore Xubuntu, but the content is equally balid for Raspberry Pi Buster Debian. 

* Part 1: Introduction to assignment 1.  

  [https://www.youtube.com/watch?v=-g6_k5siGRQ](https://www.youtube.com/watch?v=-g6_k5siGRQ)  

* Part 2: Equipping the Raspberry Pi base Virtual Machine (VM) with networking software.  

  [https://www.youtube.com/watch?v=-quDL1px6Ls](https://www.youtube.com/watch?v=-quDL1px6Ls)   

* Part 3: Drawing our network design in Visio. Network IP ID address and Host IP ID address.

  [https://www.youtube.com/watch?v=_X2-3ZNJmy4](https://www.youtube.com/watch?v=_X2-3ZNJmy4)  

* Part 4: Create and configure Raspberry Pi VMs from the Raspberry Pi base VM by cloning.  

  [https://www.youtube.com/watch?v=BX8l1fWQPUE](https://www.youtube.com/watch?v=BX8l1fWQPUE)  

* Part 5: Testing network connectivity and observing IP addresses and MAC addresses in Wireshark.

  [https://www.youtube.com/watch?v=nyWv2E39YPY](https://www.youtube.com/watch?v=nyWv2E39YPY)  

* Part 7: The ARP table maps IP addresses to MAC addresses, i.e. ARP resolves IPs to MAC addresses.  

  TBD

  []()

* Part 8: Trouble shooting the network.  

  TBD

  []()


## External video sources

* Ping network testing program for Troubleshooting:  
  [https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos](https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos)

* How to install VNC (Virtual Network Computing) server for remote desktop operation of Raspberry Pi.  

  [https://www.youtube.com/watch?v=JZ1pdVVTMrw](https://www.youtube.com/watch?v=JZ1pdVVTMrw)

## Instructor text sources

Please find the following text sources in this GitLab repository:  

[https://gitlab.com/PerPer/networking/-/tree/master/Semester_Literature/1_semester_network_literature](https://gitlab.com/PerPer/networking/-/tree/master/Semester_Literature/1_semester_network_literature)  

* raspberry Pi Buster Debian installation as a VM on on VMWW  

  [https://gitlab.com/PerPer/networking/-/blob/master/Semester_Literature/2_semester_network_literature/Raspberry/Raspberry_Installation_on_VMW_Workstation_Pi_pda_V09_p_27-47.pdf](https://gitlab.com/PerPer/networking/-/blob/master/Semester_Literature/2_semester_network_literature/Raspberry/Raspberry_Installation_on_VMW_Workstation_Pi_pda_V09_p_27-47.pdf)  

* Network manager installation. Do all the steps.

  [https://gitlab.com/PerPer/networking/-/blob/master/Semester_Literature/2_semester_network_literature/Raspberry/Raspberry_Network_Per_Dahlstroem_UCL.md](https://gitlab.com/PerPer/networking/-/blob/master/Semester_Literature/2_semester_network_literature/Raspberry/Raspberry_Network_Per_Dahlstroem_UCL.md)  

* Linux networking tools and some troubleshooting:  
[https://gitlab.com/PerPer/networking/-/tree/master/Troubleshooting](https://gitlab.com/PerPer/networking/-/tree/master/Troubleshooting)  


## External text sources

Please find the following text sources online:  

* The Linux manual pages for the  ip neighbour command:  

  [https://man7.org/linux/man-pages/man8/ip-neighbour.8.html](https://man7.org/linux/man-pages/man8/ip-neighbour.8.html)


## Configuration(s)

* TBD

## Software

Before installing software on linux do:

* update   (sudo apt update)
* upgrade   (sudo apt upgrade)

From Linux repositories:

* wireshark   (Ethernet capturing and monitoring GUI software.) 
* tcpdump   (app to capture live TCP/IP packets on a network interface)
* putty   (Terminal program.)
* net-tools   (arp, hostname, ifconfig, netstat, route).
* brctl   (Create and manipulate ethernet bridge)
* bridge-utils   (Utility to create and manage bridge devices.)
* iproute2   (ip commands like: ip route)
* curl   (curl is a command line tool to transfer data to or from a server.)
* ufw   (Uncomplicated Firewall is a program for managing a netfilter firewall)  
