---
Week: 40
Content:  Assignment 14. ARP table. ARP process. Broadcast. Broadcast area.  
Material: See links in weekly plan
Initials: PDA
---

# Week 40 Networking

## Learning goals

Please see in assignment.

## Topics to be covered

* ARP table.
* ARP process.
* Broadcast.
* Broadcast area.

##  Linux Commands:  
  
        * $ man arp (Show the arp manual.)
  
## Windows commands:  

       * NA      

## Documentation to hand in  

* Assignment 14 The ARP table and ARP process and Broadcast.  

* Please find the Assignments documents in this GitLab folder:  
[https://gitlab.com/PerPer/networking/-/tree/master/Networking_Assignments](https://gitlab.com/PerPer/networking/-/tree/master/Networking_Assignments)

* It is obligatory to follw the "Hand in requirements" stipulated in the first section of the assignments document.

* Hand in on PeerGrade. A PeerGrade notification should have been or will be send to you.  

## Schedule  

* Please see TimeEdit. Lecturing always start at 9, unless notified on ITSL.

## Instructor Instruction Videoes  

* Intstruction, theory and demonstration videos to the assignment listen in the "Assignment to hand in" section.

* Please consult the video material list:

  [https://gitlab.com/PerPer/networking/-/blob/master/Semester_Literature/1_semester_network_literature/Videoes/%23%20Index%20for%20Networking%20videoes%20on%20Youtube.md](https://gitlab.com/PerPer/networking/-/blob/master/Semester_Literature/1_semester_network_literature/Videoes/%23%20Index%20for%20Networking%20videoes%20on%20Youtube.md)  


## External video sources

* Ping network testing program for Troubleshooting:  
  [https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos](https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos)

## Instructor text sources

* Please find the following text sources in this GitLab repository:  

  [https://gitlab.com/PerPer/networking/-/tree/master/Semester_Literature/1_semester_network_literature](https://gitlab.com/PerPer/networking/-/tree/master/Semester_Literature/1_semester_network_literature)  

* Linux networking tools and some troubleshooting:  
[https://gitlab.com/PerPer/networking/-/tree/master/Troubleshooting](https://gitlab.com/PerPer/networking/-/tree/master/Troubleshooting)  


## External text sources

Please find the following text sources online:  

* The Linux manual pages for the ip neighbour command:  

  [https://man7.org/linux/man-pages/man8/ip-neighbour.8.html](https://man7.org/linux/man-pages/man8/ip-neighbour.8.html)


## Configuration(s)

* NA  

## Software

Before installing software on linux do:

* update   (sudo apt update)
* upgrade   (sudo apt upgrade)  