---
Week: 43 - 44
Content:  Assignment 4. Building a switch out of a Linux computer.
Material: See links in weekly plan
Initials: PDA
---

# Week 43-44 Networking

## Learning goals

* Please see assignment.

## Topics to be covered

* The ethernet switch.  
* The ethernet hub.  
* The Ethernet frame.  
* The IP packet.  
* Difference between an ethernet switch and an ethernet hub.  
* Wiresharking.

Linux commands:  

    $ See commands in assignment in deliverables.  

## Documentation to hand in:  

Assignment 4 The Switch. **OLA hand in week 44.**

Please note that this assignment is an Obligatory Learning Activity OLA.  
Please see the Semester Description on ucl.dk for OLA`s conditions.  

**Please hand in on both WiseFlow and on PeerGrade:**

- WiseFlow: Individual hand in.  
- PeerGrade: Team hand in.  

    Please find the assignment here: 

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/#Networking_assignments/'" type="button"> Assignments</button>  

## Schedule

Please see TimeEdit.

## Comments

Workload: 16 hour.  

## Instructor Instruction Videoes

* Intstruction, theory and demonstration videos to the assignment announced in the "Documentation to hand in" section.  

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/'" type="button"> Assignment videoes</button>  
<!--
  [https://perper.gitlab.io/networkingpages/#videoes/](https://perper.gitlab.io/networkingpages/#videoes/)
-->

## Instructor text sources  

* Please find the following text sources in this GitLab repository:  

  [https://perper.gitlab.io/networkingpages/#](https://perper.gitlab.io/networkingpages/#)  

* 18A Linux as a Switch PDA V0x.docx  
[https://perper.gitlab.io/networkingpages/switch/](https://perper.gitlab.io/networkingpages/switch/)  

<!--
* The TCP/IP stack related to the IP Packet: Only Section 6.1 in this document:  
[https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/Python_Network_programming_PDA_V08_p8-18.pdf](https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/Python_Network_programming_PDA_V08_p8-18.pdf)  

* The Linux IP networking program cheat sheet  
[https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/19A_IP_command_cheatsheet_PDA.pdf](https://gitlab.com/EAL-ITT/19a-itt1-network/blob/master/Literature/19A_IP_command_cheatsheet_PDA.pdf)  
-->


* Networking troubleshooting:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Troubleshooting</button>  

<!--
[https://perper.gitlab.io/networkingpages/#troubleshooting/](https://perper.gitlab.io/networkingpages/#troubleshooting/)
-->  

## External video sources  

* The hub vs the switch. Also the switch mac table.  
  Only the HUB from 00:00 to 01:46. Skip the BRIDGE. Only SWITCH from 02:58 to 04:31.  
[https://www.youtube.com/watch?v=eMamgWllRFY&ab_channel=CertBros](https://www.youtube.com/watch?v=eMamgWllRFY&ab_channel=CertBros)  

* The switch mechanics.  
  Only from 03:37 minutes and the rest.
  The viideo is a bit confusing to necomers as it includes ARP.  
[https://www.youtube.com/watch?v=chlBlRfsnq0&ab_channel=ISOTrainingInstitute](https://www.youtube.com/watch?v=chlBlRfsnq0&ab_channel=ISOTrainingInstitute)  

<!--
* How ARP works: Only the first two minute.  
[https://www.youtube.com/watch?v=2ydK33mPhTY&t=524s](https://www.youtube.com/watch?v=2ydK33mPhTY&t=524s)   
-->
## External text sources  

Please find the following text sources online:  

* The Linux manual pages for the ip neighbour command:  

  [https://man7.org/linux/man-pages/man8/ip-neighbour.8.html](https://man7.org/linux/man-pages/man8/ip-neighbour.8.html)  

## Configuration(s)  

* NA  

## Software  

Before installing new software, i.e. applications, on linux please do update and upgrade. Some applications depend on, i.e. are using parts off, the Linux OS and they will require the absolutely newest OS developments to function. So please do:

      $ update        ($ sudo apt update)  

      $ upgrade       ($ sudo apt upgrade)   

## Lecturer plan

1. Peer review of previous assignnment.
1. This weeks weekly plan.
1. Introduction videoe(s) to assignment.
1. VMnets vs LanSegments.
1. The new Networking information page.
