---
Week: 49 - 50
Content:  Assignment 11. Routing, two routers, five subnets. Virtual routers.  
Material: See links in weekly plan
Initials: PDA
---

# Week 49 - 50 Networking

## Learning goals

The students can at a basic level work with and explain:

* Routing between routers on virtual routers.  

## Topics to be covered

* Routing between routers on virtual routers.    

## Documentation to hand in

Assignment 11 Routing, two routers, five subnets, virtual routers.  

- PeerGrade: Individual hand in. Work in teams.  

    Please find the assignment here: 

   <button onclick="location.href='https://perper.gitlab.io/networkingpages/assignments/#assignment-11-routing-two-routers-five-subnets-virtual-routers'" type="button"> Assignment 11</button>  

## Schedule

Please see TimeEdit.  

## Comments

Workload 16 hours.

## Software

Use Putty to access the routers. Please see the video on how to connect Putty to vSRX.

## Instructor Instruction Videoes  

* <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-11-two-routers-5-subnets'" type="button"> Assignment 11 videoes</button>

## Lecturer text sources

* Networking troubleshooting:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Troubleshooting</button>  


## White Board

NA

