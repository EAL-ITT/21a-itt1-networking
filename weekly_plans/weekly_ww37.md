---
Week: 37
Content:  Assignment 2. VMnet8, Network diagrams, Linux software, static IPs and traffic monitoring.   
Material: See links in weekly plan
Initials: PDA
---

# Week 37 Networking


## Learning goals

The students can at a very basic level work with and explain:

* Set up a VMnet8 in VMWare Workstation using Virtual Network Editor.
* Set static IP address on Linux hosts.
* Use PING to check connectivity between two hosts.
* Use Wireshark to monitor traffic between two hosts.
 

### Topics to be covered

* VMWW VMnet8
* Network Diagrams. Network ID and and Host IPs.
* Two hosts - VM (Xububtu) installation.
* Set static IPs on Xubuntu in GUI.
* The ping program.
* basic IP packet traffic monitoring in Wireshark.

###  Linux Commands:  
  
    * sudo wireshark  
    * ping  
  
### Windows commands:  

    * `\>route print`  

## Deliverables  

* Assignment 2 VMnet8, Network diagrams, Linux software, static IPs and traffic monitoring.  

* Please find the Assignments documents in this GitLab folder:  
[https://gitlab.com/PerPer/networking/-/tree/master/Networking_Assignments](https://gitlab.com/PerPer/networking/-/tree/master/Networking_Assignments)

* It is obligatory to follw the "Hand in requirements" stipulated in the first section of the assignments document.

* Hand in on PeerGrade. A PeerGrade notification should have been send to you.  

* It is obligatory to use the MS Word lab report template found here for your hand in:
[https://gitlab.com/PerPer/networking/-/tree/master/Documentation_writing](https://gitlab.com/PerPer/networking/-/tree/master/Documentation_writing)

## Schedule

* Please see TimeEdit. Lecturing always start at 9, unless notified on ITSL.

## Hands-on time

* See deliverables.

## Instructor Instruction Videoes

* Intstruction, theory and demonstration videos to assignment 2. 

* Part 1: Introduction to assignment 2.  

  [https://www.youtube.com/watch?v=I_kPqqgFBCg](https://www.youtube.com/watch?v=I_kPqqgFBCg)  

* Part 2: Equipping the Xubuntu base Virtual Machine (VM) with networking software.  

  [https://www.youtube.com/watch?v=-quDL1px6Ls](https://www.youtube.com/watch?v=-quDL1px6Ls)   

* Part 3: Drawing our network design in Visio. Network IP ID address and Host IP ID address.

  [https://www.youtube.com/watch?v=_X2-3ZNJmy4](https://www.youtube.com/watch?v=_X2-3ZNJmy4)  

* Part 4: Create and configure Xubuntu VMs from the Xubuntu base VM by cloning.  

  [https://www.youtube.com/watch?v=BX8l1fWQPUE](https://www.youtube.com/watch?v=BX8l1fWQPUE)  

* Part 5: Testing network connectivity and monitoring network IP packets in Wireshark.

  [https://www.youtube.com/watch?v=nyWv2E39YPY](https://www.youtube.com/watch?v=nyWv2E39YPY)  

* Part 6: Trouble shooting the network.  

  []()


## External video sources

* Ping network testing program for Troubleshooting:  
[https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos](https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos)

## Instructor text sources

Please find the following text sources in this GitLab repository:  

[https://gitlab.com/PerPer/networking/-/tree/master/Semester_Literature/2_semester_network_literature](https://gitlab.com/PerPer/networking/-/tree/master/Semester_Literature/2_semester_network_literature) 

* Linux networking tools and some troubleshooting:  
[https://gitlab.com/PerPer/networking/-/tree/master/Troubleshooting](https://gitlab.com/PerPer/networking/-/tree/master/Troubleshooting)  


## External text sources

Please find the following text sources online:  

* TBD


## Configuration(s)

* TBD

## Software

Before in stalling software on linux do:

* update   (sudo apt update)
* upgrade   (sudo apt upgrade)

From Linux repositories:

* wireshark   (Ethernet capturing and monitoring GUI software.) 
* tcpdump   (app to capture live TCP/IP packets on a network interface)
* putty   (Terminal program.)
* net-tools   (arp, hostname, ifconfig, netstat, route).
* brctl   (Create and manipulate ethernet bridge)
* bridge-utils   (Utility to create and manage bridge devices.)
* iproute2   (ip commands like: ip route)
* curl   (curl is a command line tool to transfer data to or from a server.)
* ufw   (Uncomplicated Firewall is a program for managing a netfilter firewall)
