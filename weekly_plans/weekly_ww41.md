---
Week: 41
Content:  Assignment 15. Physical network with switch.   
Material: See links in weekly plan
Initials: PDA
---

# Week 41 Networking

## Learning goals

Please see in assignment.    

## Topics to be covered

* This weeks assignment is heavily building on the prevoiusly worked on assignments in a virtual environment. Here though the same setup is done, but now with physical devices.  
* Patch panel in Networking Lab.  

## Assignment to hand in  

* Assignment 15 Physical network with switch. 

* Please find the Assignments documents in this GitLab folder:  
[https://gitlab.com/PerPer/networking/-/tree/master/Networking_Assignments](https://gitlab.com/PerPer/networking/-/tree/master/Networking_Assignments)

* Hand in on PeerGrade. A PeerGrade notification should have been or will be send to you.  

## Schedule  

* See TimeEdit  

## Instructor Instruction Videoes

* Intstruction, theory and demonstration videos to the assignment listed in the "Assignment to hand in" section.

Assignment 15: From network design to implementation in networking lab.

* <button onclick="location.href='https://perper.gitlab.io/networkingpages/videoes/#assignment-15-from-network-design-to-implementation-in-networking-lab'" type="button"> Assignment 15 videoes</button> 
  
* Networking troubleshooting:  

  <button onclick="location.href='https://perper.gitlab.io/networkingpages/#troubleshooting/'" type="button"> Troubleshooting</button>   

## External video sources  

* Ping network testing program for Troubleshooting:  
  [https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos](https://www.youtube.com/watch?v=IIicPE38O-s&ab_channel=PowerCertAnimatedVideos)  

## Instructor text sources  

* Please find the following text sources in this GitLab repository:  

  [https://gitlab.com/PerPer/networking/-/tree/master/Semester_Literature/1_semester_network_literature](https://gitlab.com/PerPer/networking/-/tree/master/Semester_Literature/1_semester_network_literature)  

* Networking lab. How to connect from the lab tables to the networking devices, i.e. routers, switches and servers in the "data center racks" via the wall monunted patch panel.  

[]()  

* Linux networking tools and some troubleshooting:  
[https://gitlab.com/PerPer/networking/-/tree/master/Troubleshooting](https://gitlab.com/PerPer/networking/-/tree/master/Troubleshooting)  

## External text sources  

Please find the following text sources online:  

* The Linux manual pages for the ip neighbour command:  

  [https://man7.org/linux/man-pages/man8/ip-neighbour.8.html](https://man7.org/linux/man-pages/man8/ip-neighbour.8.html)  

## Configuration(s)  

* NA  

## Software  

Before installing software on linux please do:  

* update   (sudo apt update)  
* upgrade   (sudo apt upgrade)   
