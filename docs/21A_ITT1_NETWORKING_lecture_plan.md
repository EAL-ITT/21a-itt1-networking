---
title: 'Subject plan ITT1 Networking'
subtitle: 'Lecture plan'
filename: '21A_ITT1_NETWORKING_lecture_plan'
authors: ['Per Dahlstrøm \<pda@ucl.dk\>']
main_author: 'Per Dahlstrøm'
email: 'pda@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
semester: 21A
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, itoei211 , itoei212, 21A
* Name of lecturer and date of filling in form: PDA 2021-08-01
* Title of the course, module or project and ECTS: Networking, 6 ECTS


See weekly plan for details like detailed daily plan, links, references, exercises and so on.


| Lecturer and Content |  Week | Description |
| :---: | :---: | :---: | 
|  PDA, Workstation and Linux VM | 36 | Preparation: Read/Watch material |
|  PDA, Network diagrams | 37 | IP adresses on Linux. Workstation VMnets. Preparation: Read/Watch material |
|  PDA, The switch and the ARP | 38 | Preparation: Read/Watch material |
|  PDA, The switch and STP | 39 | The switch and STP. Preparation: Read/Watch material |
|  PDA, Routing on a virtual Juniper SRX240 called vSRX | 40 | Preparation: Read/Watch material |
|  PDA, Routing between routers on virtual routers | 41 | Preparation: Read/Watch material |
|  PDA, Catch up | 42 | Holiday. Preparation: Read/Watch material for catch up |

# General info about the course, module or project

The purpose of the course is to make the student able to:

* elaborate and implement network configurations.

## The student’s learning outcome

At the end of the course, the student can:

* elaborate and implement network configurations.

## Content

The course is formed as a sequence of hands on exercises, which will bring the student more and more knowledge and practical skills.

## Method

The course is formed as a sequence of hands on exercises, mainly conducted as flipped class room,  which will bring the student more and more knowledge and practical skills.

## Equipment

The routers, servers and switches in the networking lab.

## Projects with external collaborators (proportion of students who participated)
None at this time.

## Test form/assessment

The course includes 1 Obligatory Learning Activity.

Assessment of the course is part of the "1st year exam - 1st part" held at the end of 1st semester.

See "1st semester description 2021" for details on obligatory learning activities and examination, online at [https://www.ucl.dk/international/full-degree/study-documents#it+technology](https://www.ucl.dk/international/full-degree/study-documents#it+technology)

## Study activity model

![study activity model](Study_Activity_Model.png)

*Generate your own .png study activity model at [https://danskeprofessionshøjskoler.dk/studieaktivitetsmodel-2/](https://danskeprofessionshøjskoler.dk/studieaktivitetsmodel-2/) and replace the image*

## Other general information
None at this time.
